from django.urls import include, path

from rest_framework import routers

from .viewsets import TextViewSet

app_name = "app"

router_v1 = routers.DefaultRouter()
router_v1.register(r"text", TextViewSet, basename="text")

urlpatterns = [
    path("api/v1/", include(router_v1.urls)),
]
