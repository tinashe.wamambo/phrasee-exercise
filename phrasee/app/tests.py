from unittest.mock import patch

from django.test import Client, TestCase

MODULE = "phrasee.app.viewsets"


class TextViewSetTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.client = Client()

    def setUp(self):
        super().setUp()

        randint_patcher = patch(f"{MODULE}.random.randint")
        self.mock_randint = randint_patcher.start()
        self.addCleanup(randint_patcher.stop)

    def test_text_viewset(self):
        self.mock_randint.return_value = 10

        response = self.client.get("/api/v1/text/")

        self.assertEqual(200, response.status_code)

        json_response = response.json()

        self.assertEqual("I would like 10 hippopotamus please", json_response["phrase"])
        self.assertEqual(False, json_response["is_palindrome"])

    def test_text_viewset_with_input_string(self):
        self.mock_randint.return_value = 10

        response = self.client.get("/api/v1/text/?input_string=radar")

        self.assertEqual(200, response.status_code)

        json_response = response.json()

        self.assertEqual("I would like 10 radar please", json_response["phrase"])
        self.assertEqual(True, json_response["is_palindrome"])

    def test_text_viewset_fail_input_string_not_alphanumeric(self):
        self.mock_randint.return_value = 10

        response = self.client.get("/api/v1/text/?input_string=radar!!!")

        self.assertEqual(403, response.status_code)

        json_response = response.json()

        self.assertEqual(
            "'input_string' must only contain alphanumeric characters",
            json_response["error"],
        )
