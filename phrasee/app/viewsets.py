from numpy import random
from rest_framework import serializers, viewsets
from rest_framework.response import Response


class Text:
    def __init__(self, random_integer, input_string):
        self.phrase = f"I would like {random_integer} {input_string} please"
        self.is_palindrome = self.is_palindrome(input_string)

    def is_palindrome(self, input_string):
        """Check if input string is a palindrome"""
        input_string_reversed = "".join(reversed(input_string))

        return bool(input_string == input_string_reversed)


class TextSerializer(serializers.Serializer):
    phrase = serializers.CharField(max_length=20)
    is_palindrome = serializers.BooleanField(read_only=True)


class TextViewSet(viewsets.ViewSet):
    required_scopes = ["read"]

    def list(self, request):
        input_string = request.GET.get("input_string", "hippopotamus")

        if not input_string.isalnum():
            return Response(
                {"error": "'input_string' must only contain alphanumeric characters"},
                status=403,
            )

        random_integer = random.randint(100)
        text = Text(random_integer, input_string)

        serializer = TextSerializer(text)

        return Response(serializer.data)
