from .base import *  # noqa

ENVIRONMENT = "docker"

SECRET_KEY = os.environ.get(  # noqa
    "SECRET_KEY", "8ixbyom5pkp5k8cwb*40tpjt+$n^q2v9^i!&+a3z%!&!%+hb4c"
)
DEBUG = bool(int(os.environ.get("DEBUG", 1)))  # noqa
