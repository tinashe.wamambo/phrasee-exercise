# Phrasee-Exercise

This is a Phrasee technical exercise

## Technology Stack

It is actively developed using Python and the Django Framework.

Requires:

- Docker
- Postgres = 12.3
- Python >= 3.6
- Django ~= 3.0

## Development

This application can be developed in a Docker environment or virtual environment.

### Docker Environment

The application uses a docker PostgreSQL database instance.
Install the build prerequisites required to run `psycopg2`.

At the time of writing, the link below provides details to setup the required
build prerequisites:

http://initd.org/psycopg/docs/install.html

You do not have to install the postgres client at system level.

To bring up the docker environment: `make up`

To bring down the docker environment: `make down`

### Virtual Environment

Create a virtual environment and install dependencies.

```
pip install -r requirements.txt
```

### Run Development Server

Even when running the app outside docker, you still need to run `make up` so the database is created.

Export environment variables:

```
export DATABASE_URL=postgres://postgres:postgres@localhost/phrasee
export DJANGO_SETTINGS_MODULE=phrasee.settings.local
```

Run the Django development server

Use python directly

```
python manage.py runserver
```

### Usage

To access the REST API endpoint using the default input string: `http://localhost:8000/api/v1/text/`

You can supply a custom input string: `http://localhost:8000/api/v1/text/?input_string=radar`

You can supply a non alphanumeric input string: `http://localhost:8000/api/v1/text/?input_string=rada!!!`

### Run Tests

Install tox: `pip install tox`

`tox` is used to test the application

Run the unit tests

Use python directly

```
python manage.py test --parallel --failfast
```

Run tests in a docker environment

```
make test
```

### Code QA

Install pre-commit: `pip install pre-commit`

`pre-commit` is used to run checks on the codebase before commits are
made to `git`.

To run checks: `make check`

You can invoke `pre-commit` directly:`pre-commit run --all-files`

For more information: https://pre-commit.com/

### Manage Dependencies

`pip-tools` is used manage application dependencies

For more information: https://github.com/jazzband/pip-tools

Install pip-tools

```
pip install pip-tools
```

Add a new dependency to `requirements.in` and then run:

```
make requirements
```

Upgrade dependencies

```
make upgrade-requirements
```

Sync dependencies in virtual environment to reflect upgrades

```
make sync-requirements
```

## CI/CD

CI/CD is handled by Gitlab CI. See: `.gitlab-ci.yml` file

## Ansible Deployment

### Prerequisites

This app is deployed on a server instance running Ubuntu 20.04 LTS.

The following steps must be taken to setup the server:

    1. Add user `phrasee_deploy`: `adduser phrasee_deploy`
    2. Add `phrasee_deploy` to sudo group: `usermod -aG sudo phrasee_deploy`

    3. Add `phrasee_deploy` to bottom of `/etc/sudoers` file:

    ```
    phrasee_deploy  ALL=(ALL) NOPASSWD:ALL
    ```

    4. Add `phrasee_deploy` private key to the authorized keys file found here: `/home/phrasee_deploy/.ssh/authorized_keys`

    4. Install `Docker` onto server: https://docs.docker.com/engine/install/ubuntu/

### Set Up Let's Encrypt Certificate using Docker Certbot

If the nginx container is running, stop the container.

Make sure you have a DNS A record mapped to the proxy server’s public IP address. Then, on your proxy server, provision a staging version of the certificates using the certbot Docker image.

```
docker run -it --rm -p 80:80 --name certbot -v "/etc/letsencrypt:/etc/letsencrypt" -v "/var/lib/letsencrypt:/var/lib/letsencrypt" certbot/certbot certonly --standalone --staging -d <your_domain.com> --email <your email> --agree-tos --no-eff-email
```

Restart Nginx, then navigate to http://your_domain.com. You may receive a warning in your browser that the certificate authority is invalid. This is expected as we’ve provisioned staging certificates and not production Let’s Encrypt certificates. Check the URL bar of your browser to confirm that your HTTP request was redirected to HTTPS.

Stop Nginx again and run the certbot client again, this time omitting the --staging flag:

```
docker run -it --rm -p 80:80 --name certbot -v "/etc/letsencrypt:/etc/letsencrypt" -v "/var/lib/letsencrypt:/var/lib/letsencrypt" certbot/certbot certonly --standalone -d <your_domain.com> --email <your email> --agree-tos --no-eff-email
```

### Set Up Let's Encrypt Certificate Renewal

Open crontab file on server: `sudo crontab -e`

Add the following to the bottom of the file:

```
0 0 * * * docker run -it --rm -p 80:80 --name certbot -v "/etc/letsencrypt:/etc/letsencrypt" -v "/var/lib/letsencrypt:/var/lib/letsencrypt" certbot/certbot renew --agree-tos
```

### Deploy App

Phrasee is currently deployed using Ansible.

    1. Get the deployment credentials
    2. Navigate to the `deploy` directory
    3. Create/activate a phrasee deployment virtual environment
    4. Install deployment requirements: `pip install -r deploy-requirements.txt`

To deploy app:

```
APP_VERSION=<version> ansible-playbook -i inventory/production playbook.yml
```

`<version>` is the version of the app you want to deploy. This is a git release tag.

Secrets are stored in the ansible vault for each inventory

To encrypt secrets in ansible vault:

```
ansible-vault encrypt <inventory>/group_vars/all/vault.yml
```

To decrypt secrets in ansible vault:

```
ansible-vault decrypt <inventory>/group_vars/all/vault.yml
```
