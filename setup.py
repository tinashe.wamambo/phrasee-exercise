from setuptools import find_packages, setup

setup(
    name="phrasee",
    use_scm_version=True,
    setup_requires=["setuptools_scm"],
    author="Tinashe Wamambo",
    author_email="tinashe.wamambo@hotmail.co.uk",
    url="https://gitlab.com/tinashe.wamambo/phrasee-exercise",
    packages=find_packages(
        exclude=[
            "phrasee.app.tests",
        ]
    ),
    python_requires=">=3.6",
    install_requires=[
        "dj-database-url",
        "Django~=3.0",
        "djangorestframework",
        "numpy",
    ],
    extras_require={
        "psycopg2": ["psycopg2"],
        "psycopg2-binary": ["psycopg2-binary"],
        "testing": ["factory-boy"],
    },
    classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: Web Environment",
        "Framework :: Django",
        "Framework :: Django :: 3.0",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
    ],
    scripts=["manage.py"],
)
